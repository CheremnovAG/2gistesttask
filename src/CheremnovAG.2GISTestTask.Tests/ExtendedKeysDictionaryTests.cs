﻿using CheremnovAG._2GISTestTask.Core;
using NUnit.Framework;
using System.Threading.Tasks;

namespace CheremnovAG._2GISTestTask.Tests
{
    [TestFixture]
    public class TestNunitTests
    {
        [TestCase(3)]
        public void TestGetCount_Expected3(int expectedCount)
        {
            var myCollection = new ExtendedKeysDictionary<int, string, int>
            {
                { 1, "Иван", 4 },
                { 3, "Иван", 5 },
                { 5, "Константин", 6 }
            };

            var count = 0;
            foreach (var item in myCollection)
            {
                count++;
            }

            Assert.AreEqual(expectedCount, count);
        }

        [TestCase(8, 9)]
        [TestCase(54, 92)]
        [TestCase(11, 23)]
        public void TestIndexator_ExpectedIntValue(int expectedValue1, int expectedValue2)
        {
            var myCollection = new ExtendedKeysDictionary<int, int, int>
            {
                [1, 2] = expectedValue1,
                [3, 4] = expectedValue2
            };

            Assert.AreEqual(expectedValue1, myCollection[1, 2]);
            Assert.AreEqual(expectedValue2, myCollection[3, 4]);
        }

        [TestCase(6, 4)]
        [TestCase(11, 12)]
        [TestCase(53, 89)]
        public void TestAccessById_ExpectedStringValue(int expectedValue1, int expectedValue2)
        {
            var myCollection = new ExtendedKeysDictionary<string, int, int>
            {
                { "Иван", 1, expectedValue1 },
                { "Иван", 3, expectedValue2 },
                { "Константин", 5, 6 }
            };

            Assert.AreEqual(new int[] { expectedValue1, expectedValue2 }, myCollection["Иван"]);
        }

        [TestCase(6, 4)]
        [TestCase(11, 12)]
        [TestCase(53, 89)]
        public void TestAccessByName_ExpectedStringValue(int expectedValue1, int expectedValue2)
        {
            var myCollection = new ExtendedKeysDictionary<int, string, int>
            {
                { 1, "Иван", expectedValue1 },
                { 3, "Иван", expectedValue2 },
                { 5, "Константин", 6 }
            };

            Assert.AreEqual(new int[] { expectedValue1, expectedValue2 }, myCollection["Иван"]);
        }
        
        [TestCase(221445)]
        public void TestParallelUpdate(int expectedValue)
        {
            var myCollection = new ExtendedKeysDictionary<int, string, int>
            {
                { 5, "Тест", 0 }
            };

            Parallel.For(0, 666,
                (i) =>
                {
                    myCollection.EnterUpgradeableReadLock();
                    myCollection[5, "Тест"] += i;
                    myCollection.ExitUpgradeableReadLock();
                });

            Assert.AreEqual(expectedValue, myCollection[5, "Тест"]);
        }
    }
}