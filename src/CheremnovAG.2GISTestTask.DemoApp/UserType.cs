﻿namespace CheremnovAG._2GISTestTask.DemoApp
{
    /// <summary>
    /// Пользовательский тип
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        public UserType(int age, string name)
        {
            Age = age;
            Name = name;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is UserType record ? Age.Equals(record.Age) && Name.Equals(record.Name) : false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Age.GetHashCode() ^ Name.GetHashCode();
        }

        /// <inheritdoc />
        public override string ToString()   //Для отладки
        {
            return $"[{Name.ToString()}, {Age.ToString()}]";
        }
    }
}