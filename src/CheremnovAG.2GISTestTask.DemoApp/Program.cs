﻿using CheremnovAG._2GISTestTask.Core;
using System;

namespace CheremnovAG._2GISTestTask.DemoApp
{
    public class Program
    {
        public static void Main()
        {
            var myCollection = new ExtendedKeysDictionary<UserType, int, string>();

            #region Заполнение данных
            myCollection.Add(new UserType(41, "Леонард"), 2454, "Атташе");
            myCollection.Add(new UserType(55, "Максимильян"), 2044, "Букмекер");
            myCollection.Add(new UserType(40, "Мстислав"), 5751, "Брокер");

            myCollection[new UserType(34, "Альберт"), 2398] = "Программист";
            myCollection[new UserType(34, "Альберт"), 9197] = "Ведущий программист";
            myCollection[new UserType(43, "Боримир"), 9428] = "Библиотекарь";
            myCollection[new UserType(56, "Варфоломей"), 5648] = "Балетмейстер";
            myCollection[new UserType(57, "Ибрагим"), 6179] = "Библиограф";
            myCollection[new UserType(63, "Кондратий"), 3452] = "Врач";
            myCollection[new UserType(46, "Клементий"), 5648] = "Пожарный";
            myCollection[new UserType(43, "Ларион"), 3519] = "Гидрохимик";
            #endregion

            Console.WriteLine("Все записи:");
            foreach (var item in myCollection)
            {
                Console.WriteLine($"*Пользователь* = {item.Key} *Должность* = {item.Value}");
            }

            Console.WriteLine();
            Console.WriteLine("Ибрагим #id = 6179 занимает следующую должность: " + myCollection[new UserType(57, "Ибрагим"), 6179]);
            Console.WriteLine();
                      
            Console.WriteLine("Должности людей с id = 5648:");
            foreach (var item in myCollection[5648])
            {
                Console.WriteLine($" --> {item}");
            }
            Console.WriteLine();

            Console.WriteLine("{\"Альберт\", 34} занимают должности:");
            foreach (var item in myCollection[new UserType(34, "Альберт")])
            {
                Console.WriteLine($" --> {item}");
            }
        }
    }
}