﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CheremnovAG._2GISTestTask.Core
{
    /// <summary>
    /// Класс-коллекция для хранения элементов, имеющих уникальный составной ключ [Id, Name]
    /// </summary>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    /// <typeparam name="TName">Тип имени</typeparam>
    /// <typeparam name="TValue">Тип значения</typeparam>
    public class ExtendedKeysDictionary<TId, TName, TValue> : IDictionary<Tuple<TId, TName>, TValue>
    {
        #region Приватные поля
        private Dictionary<Tuple<TId, TName>, TValue> _mainDictionary;
        private Dictionary<TId, List<TValue>> _cachedByIdDictionary;
        private Dictionary<TName, List<TValue>> _cachedByNameDictionary;

        private readonly ReaderWriterLockSlim _readAndWriteLocker = new ReaderWriterLockSlim();
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="size">Размер</param>
        public ExtendedKeysDictionary(uint size = 0)
        {
            _mainDictionary = new Dictionary<Tuple<TId, TName>, TValue>((int)size);
            _cachedByIdDictionary = new Dictionary<TId, List<TValue>>((int)size);
            _cachedByNameDictionary = new Dictionary<TName, List<TValue>>((int)size);
        }

        #region Интерфейсные методы
        /// <inheritdoc />
        public TValue this[Tuple<TId, TName> key]
        {
            get
            {
                try
                {
                    _readAndWriteLocker.EnterReadLock();

                    return this[key];
                }
                finally
                {
                    _readAndWriteLocker.ExitReadLock();
                }
            }
            set => Add(key, value);
        }

        /// <inheritdoc />
        public ICollection<Tuple<TId, TName>> Keys
        {
            get
            {
                try
                {
                    _readAndWriteLocker.EnterReadLock();

                    return _mainDictionary.Keys;
                }
                finally
                {
                    _readAndWriteLocker.ExitReadLock();
                }
            }
        }

        /// <inheritdoc />
        public ICollection<TValue> Values
        {
            get
            {
                try
                {
                    _readAndWriteLocker.EnterReadLock();

                    return _mainDictionary.Values;
                }
                finally
                {
                    _readAndWriteLocker.ExitReadLock();
                }
            }
        }

        /// <inheritdoc />
        public int Count
        {
            get
            {
                try
                {
                    _readAndWriteLocker.EnterReadLock();

                    return _mainDictionary.Count;
                }
                finally
                {
                    _readAndWriteLocker.ExitReadLock();
                }
            }
        }

        /// <inheritdoc />
        /// Решено было возвращать по-умолчанию false, чтобы не ожидать окончания всех блокировок чтения.
        public bool IsReadOnly => false;

        /// <inheritdoc />
        public void Add(Tuple<TId, TName> key, TValue value)
        {
            try
            {
                _readAndWriteLocker.EnterWriteLock();
                _mainDictionary[key] = value;

                AddToCache(_cachedByIdDictionary, key.Item1, value);
                AddToCache(_cachedByNameDictionary, key.Item2, value);
            }
            finally
            {
                _readAndWriteLocker.ExitWriteLock();
            }
        }

        /// <inheritdoc />
        public void Add(KeyValuePair<Tuple<TId, TName>, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        /// <inheritdoc />
        public void Clear()
        {
            try
            {
                _readAndWriteLocker.EnterWriteLock();

                _mainDictionary.Clear();
                _cachedByIdDictionary.Clear();
                _cachedByNameDictionary.Clear();
            }
            finally
            {
                _readAndWriteLocker.ExitWriteLock();
            }
        }

        /// <inheritdoc />
        public bool Contains(KeyValuePair<Tuple<TId, TName>, TValue> item)
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return _mainDictionary.ContainsKey(item.Key);
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }

        /// <inheritdoc />
        public bool ContainsKey(Tuple<TId, TName> key)
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return _mainDictionary.ContainsKey(key);
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }

        /// <inheritdoc />
        public void CopyTo(KeyValuePair<Tuple<TId, TName>, TValue>[] array, int arrayIndex)
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                for (var index = arrayIndex; index < _mainDictionary.Count; index++)
                {
                    array[index] = _mainDictionary.ElementAt(index);
                }
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }

        /// <inheritdoc />
        public IEnumerator<KeyValuePair<Tuple<TId, TName>, TValue>> GetEnumerator()
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return _mainDictionary.GetEnumerator();
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }

        /// <inheritdoc />
        public bool Remove(Tuple<TId, TName> key)
        {
            try
            {
                _readAndWriteLocker.EnterUpgradeableReadLock();

                var id = key.Item1;
                var name = key.Item2;
                var result = false;

                /// Если такой ключ есть
                if (_mainDictionary.ContainsKey(new Tuple<TId, TName>(id, name)))
                {
                    try
                    {
                        _readAndWriteLocker.EnterWriteLock();                             

                        var tempKey = new Tuple<TId, TName>(id, name);

                        RemoveFromCache(_cachedByIdDictionary, key.Item1, tempKey);
                        RemoveFromCache(_cachedByNameDictionary, key.Item2, tempKey);

                        result = true;
                    }
                    finally
                    {
                        _readAndWriteLocker.ExitWriteLock();
                    }
                }

                return result;
            }
            finally
            {
                _readAndWriteLocker.ExitUpgradeableReadLock();
            }
        }

        /// <inheritdoc />
        public bool Remove(KeyValuePair<Tuple<TId, TName>, TValue> item) => Remove(item.Key);

        /// <inheritdoc />
        public bool TryGetValue(Tuple<TId, TName> key, out TValue value)
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return _mainDictionary.TryGetValue(key, out value);
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return _mainDictionary.GetEnumerator();
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }
        #endregion
        
        #region Методы по работе с кешем
        private void AddToCache<TKeyItem>(Dictionary<TKeyItem, List<TValue>> dictionary, TKeyItem keyItem, TValue value)
        {
            if (dictionary.ContainsKey(keyItem))
            {
                dictionary[keyItem].Add(value);
            }
            else
            {
                dictionary[keyItem] = new List<TValue>() { value };
            }
        }
        
        private void RemoveFromCache<TKeyItem>(Dictionary<TKeyItem, List<TValue>> dictionary, TKeyItem keyItem, Tuple<TId, TName> tempKey)
        {
            if (dictionary[keyItem].Count > 1)
            {
                dictionary[keyItem].Remove(_mainDictionary[tempKey]);
            }
            else
            {
                dictionary.Remove(keyItem);
            }
        }
        #endregion

        #region Добавленые для удобства методы
        /// <summary>
        /// Получение элементов по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Коллекция отобранных элементов</returns>
        public TValue[] this[TId id] => GetBySimpleKey(_cachedByIdDictionary, id);

        /// <summary>
        /// Получение элементов по имени
        /// </summary>
        /// <param name="name">Имя</param>
        /// <returns>Коллекция отобранных элементов</returns>
        public TValue[] this[TName name] => GetBySimpleKey(_cachedByNameDictionary, name);

        /// <summary>
        /// Получение элемента по составному ключу
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="name">Имя</param>
        /// <returns>Элемент</returns>
        public TValue this[TId id, TName name]
        {
            set => Add(id, name, value);
            get
            {
                try
                {
                    _readAndWriteLocker.EnterReadLock();
                    return _mainDictionary[new Tuple<TId, TName>(id, name)];
                }
                finally
                {
                    _readAndWriteLocker.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Добавить запись к коллекции
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="name">Имя</param>
        /// <param name="value">Значение</param>
        public void Add(TId id, TName name, TValue value) => Add(new Tuple<TId, TName>(id, name), value);
                       
        private TValue[] GetBySimpleKey<TKey>(Dictionary<TKey, List<TValue>> dictionary, TKey key)
        {
            try
            {
                _readAndWriteLocker.EnterReadLock();

                return dictionary[key].ToArray();
            }
            finally
            {
                _readAndWriteLocker.ExitReadLock();
            }
        }
        #endregion

        #region Ручной захват для обеспечения работы с неатомарными операциями
        /// <summary>
        /// Tries to enter the lock in upgradable mode.
        /// </summary>
        public void EnterUpgradeableReadLock() => _readAndWriteLocker.EnterUpgradeableReadLock();
                
        /// <summary>
        /// Reduces the recursion count for upgradeable mode, and exits upgradeable mode if the resulting count is 0 (zero).
        /// </summary>
        public void ExitUpgradeableReadLock() => _readAndWriteLocker.ExitUpgradeableReadLock();
        #endregion
    }
}